# odin-landing-page


## Description
This project has been done as a part of [The Odin Project](https://www.theodinproject.com/) coursework. It has been done just for learning purposes.

## Images used
- [Buildings on grass fields near mountains](https://www.pexels.com/photo/buildings-on-grass-field-near-mountains-2832026/) by [Felix Mittermeier](https://www.pexels.com/@felixmittermeier/), taken from [Pexels](https://www.pexels.com/)
- [Photo of snow capped mountain during evening](https://www.pexels.com/photo/photo-of-snow-capped-mountain-during-evening-2437286/) by [Eberhard Grossgasteiger](https://www.pexels.com/@eberhardgross/), taken from [Pexels](https://www.pexels.com/)
- [Photo of bridge under white clouds](https://www.pexels.com/photo/photo-of-bridge-under-white-clouds-3493777/) by [Aleksey Kuprikov](https://www.pexels.com/@aleksey-kuprikov-1883853/), taken from [Pexels](https://www.pexels.com/)
- [Landscape field and trees](https://www.pexels.com/photo/landscape-field-and-trees-3264706/) by [Simon Berger](https://www.pexels.com/@8moments/), taken from [Pexels](https://www.pexels.com/)
- [Aerial photo of forest under blue sky](https://www.pexels.com/photo/aerial-photo-of-forest-under-blue-sky-288102/) by [Curioso Photography](https://www.pexels.com/@curiosophotography/), taken from [Pexels](https://www.pexels.com/)
